local opt = vim.opt
local g = vim.g

--search
if vim.fn.executable('rg') then
     opt.grepprg = 'rg --no-heading --vimgrep'
     opt.grepformat = '%f:%l:%c:%m'
end

-- misc
opt.title = true
opt.clipboard = "unnamedplus"
opt.cmdheight = 1
opt.cul = true -- cursor line

-- Indentline
opt.expandtab = true
opt.shiftwidth = 2
opt.smartindent = true

opt.hidden = true
opt.ignorecase = true
opt.smartcase = true
opt.mouse = 'a'
opt.foldmethod = 'marker'

-- Wildmenu
opt.wildmenu = false
opt.wildmode = 'list:longest'

-- Numbers
opt.number = true
opt.numberwidth = 4
opt.relativenumber = true
opt.ruler = false
opt.colorcolumn = "100"

-- disable nvim intro
opt.shortmess:append "sI"

opt.signcolumn = "yes"
opt.splitbelow = true
opt.splitright = true
opt.tabstop = 8
opt.timeoutlen = 400
opt.undofile = true

opt.listchars = 'nbsp:¬,extends:»,precedes:«,trail:•'

-- interval for writing swap file to disk, also used by gitsigns
opt.updatetime = 250

-- go to previous/next line with h,l,left arrow and right arrow
-- when cursor reaches end/beginning of line
opt.whichwrap:append "<>[]hl"

g.markdown_fenced_languages = {'cpp', 'sql', 'rust'}

-- colors
opt.termguicolors = true
opt.background = [[dark]]
g.tokyonight_style = 'night'
vim.cmd [[colo nord]]

-- Disable Builtins
local disabled_built_ins = {
  "2html_plugin",
  "getscript",
  "getscriptPlugin",
  "gzip",
  "logipat",
  "netrw",
  "netrwPlugin",
  "netrwSettings",
  "netrwFileHandlers",
  "matchit",
  "tar",
  "tarPlugin",
  "rrhelper",
  "spellfile_plugin",
  "vimball",
  "vimballPlugin",
  "zip",
  "zipPlugin",
}

for _, plugin in pairs(disabled_built_ins) do
  g["loaded_" .. plugin] = 1
end

