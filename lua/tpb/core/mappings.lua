local map = vim.api.nvim_set_keymap
local is_windows = require('tpb.core.vars').is_windows
local opts = { silent = true, noremap = true }
map('n', '<space>', '<nop>', opts)
vim.g.mapleader = " "
-- remaps
-- make macros faster by disabling autocmds they might trigger
-- breaks some plugins apparently
-- vim.cmd [[xnoremap @ :<C-U>execute "noautocmd '<,'>norm! " . v:count1 . "@" . getcharstr()<cr>]]

-- Builtin
-- buffers
map('n', '<Leader>bn', ':bn<CR>',opts)
map('n', '<Leader>bp', ':bp<CR>', opts)
map('n', '<Leader>bq', ':bd<CR>', opts)
map('n', '<Leader>bQ', ':bd!<CR>', opts)
map('n', '<Leader>bs', ':w<CR>', opts)
map('n', '<Leader><Leader>', '<C-^>', opts)
-- window
map('n', '<Leader>wv', ':vs<CR>', opts)
map('n', '<Leader>ws', ':split<CR>', opts)
map('n', '<Leader>wq', ':wq<CR>', opts)
map('n', '<Leader>wo', '<C-w>o', opts)
map('n', '<Leader>wl', '<C-w>l', opts)
map('n', '<Leader>wh', '<C-w>h', opts)
map('n', '<Leader>wk', '<C-w>k', opts)
map('n', '<Leader>wj', '<C-w>j', opts)
-- files
map('n', '<Leader>fs', ':w<CR>', opts)
map('n', '<Leader>op', ':NvimTreeToggle<CR>', opts)
-- Lua
map('n', '<Leader>ll', ':luafile %<CR>', opts)
-- Packer
map('n', '<Leader>ps', ':PackerSync<CR>', opts)
map('n', '<Leader>pc', ':PackerCompile<CR>', opts)
-- quickfix
map('n', '<Leader>cc', ':cclo<CR>', opts)
map('n', '<Leader>co', ':copen<CR>', opts)
map('n', '<Leader>cp', ':colder<CR>', opts)
map('n', '<Leader>cn', ':cnewer<CR>', opts)
--misc
map('n', '<Leader>hh', ':cd %:p:h<CR>', opts)
if is_windows then
  map('n', '<Leader>hw', ':lua require("tpb.secrets").cdWorkNotes()<CR>', opts)
end
map('n', '<Leader>tz', ':ZenMode<CR>', opts)

-- Plugins
-- Fuzzy
map('n', '<Leader>fr', '<cmd>Telescope oldfiles<CR>', opts)
map('n', '<Leader>sg', '<cmd>Telescope live_grep<CR>', opts)
map('n', '<Leader>ss', '<cmd>Telescope grep_string<CR>', opts)
map('n', '<Leader>sb', '<cmd>Telescope current_buffer_fuzzy_find<CR>', opts)
map('n', '<Leader>bb', '<cmd>Telescope buffers<CR>', opts)
map('n', '<Leader>ff', '<cmd>Telescope find_files<CR>', opts)
map('n', '<Leader>tt', '<cmd>Telescope colorscheme<CR>', opts)
map('n', '<Leader>fg', '<cmd>Telescope git_files<CR>', opts)
map('n', '<Leader>sq', '<cmd>Telescope quickfix<CR>', opts)

-- Lazygit
map('n', '<Leader>gg', '<cmd>LazyGit<CR>', opts)

-- Dadbod
if is_windows then
  map('n', '<Leader>ds', ':lua require("tpb.secrets").setDB("S")<CR>', opts)
  map('n', '<Leader>dm', ':lua require("tpb.secrets").setDB("M")<CR>', opts)
end
