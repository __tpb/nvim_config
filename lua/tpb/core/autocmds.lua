local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

local load_lualine = function(theme)
  require('lualine').setup({ options = {
    theme = theme,
    globalstatus = true,
    component_separators = { left = '', right = '' },
    section_separators = { left = '', right = '' }
  } })
end

autocmd('ColorScheme', {
  pattern = 'nord',
  desc = 'compensate for nord theme contrast option not working and load lualine',
  group = augroup('nord', { clear = true }),
  callback = function()
    vim.api.nvim_set_hl(0, 'VertSplit', { link = 'IndentBlankLineChar' })
    load_lualine('nord')
  end,
})

autocmd('ColorScheme', {
  pattern = 'seoul256',
  desc = 'Automatically load lualine theme',
  group = augroup('seoul256', { clear = true }),
  callback = function()
    load_lualine('seoul256')
  end,
})

autocmd('ColorScheme', {
  pattern = 'tokyonight',
  desc = 'Automatically load lualine theme',
  group = augroup('tokyonight', { clear = true }),
  callback = function()
    load_lualine('tokyonight')
  end,
})

autocmd('ColorScheme', {
  pattern = 'wind,melange',
  desc = 'Set lualine theme to auto for unsupported themes',
  group = augroup('no_lualine', { clear = true }),
  callback = function()
    load_lualine('auto')
  end,
})

autocmd('ColorScheme', {
  pattern = 'vscode',
  desc = 'Match vscode better and automatically load lualine theme',
  group = augroup('vscode', { clear = true }),
  callback = function()
    load_lualine('vscode')
    if vim.g.vscode_style == 'dark' then
      vim.api.nvim_set_hl(0, 'LineNr', { fg = '#858585' })
      vim.api.nvim_set_hl(0, 'CursorLineNr', { fg = '#C6C6C6' })
    end
  end,
})

autocmd('FileType', {
  pattern = 'sql,mysql,plsql',
  group = augroup('dadbod', { clear = true }),
  callback = function()
    require('cmp').setup.buffer({ sources = { { name = 'vim-dadbod-completion' } } })
  end
})

autocmd("BufWritePre", {
  pattern = "*",
  desc = 'Strip trailing whitespaces on save',
  group = augroup('bufwrite', { clear = true }),
  command = "%s/\\s\\+$//e"
})
