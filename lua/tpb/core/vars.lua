
local vars = {}
local home    = os.getenv("HOME")
local path_sep = vars.is_windows and '\\' or '/'
local os_name = vim.loop.os_uname().sysname

function vars:load_variables()
  self.is_linux   = os_name == 'Linux'
  self.is_windows = os_name == 'Windows_NT'
  self.vim_path    = vim.fn.stdpath('config')
  self.cache_dir   = home .. path_sep..'.cache'..path_sep..'nvim'..path_sep
  self.modules_dir = self.vim_path .. path_sep..'modules'
  self.path_sep = path_sep
  self.home = home
  self.data_dir = string.format('%s/site/',vim.fn.stdpath('data'))
end

vars:load_variables()

return vars
