vim.cmd [[packadd packer.nvim]]
return require('packer').startup(function(use)
  use {'wbthomason/packer.nvim',
  opt = true,
  cmd = {'PackerSync', 'PackerCompile'}}
  use {'lewis6991/impatient.nvim'}

  -- Telescope
  use {'nvim-telescope/telescope.nvim',
  requires = {{'nvim-lua/plenary.nvim'}},
  cmd = 'Telescope',
  config = function() require('tpb.plugins.telescope').setup() end}
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make'}

  -- Treesitter
  use {'nvim-treesitter/nvim-treesitter'
  ,run = ':TSUpdate'
  ,config = function() require('tpb.plugins.treesitter') end}
  use {'nvim-treesitter/playground'}

  -- LSP
  use {'neovim/nvim-lspconfig',
  config = function() require('tpb.plugins.lsp') end}
  use {'folke/lua-dev.nvim'}
  use {'onsails/lspkind-nvim'}

  -- Completion
  use {'hrsh7th/cmp-nvim-lsp'}
  use {'hrsh7th/cmp-buffer'}
  use {'hrsh7th/cmp-path'}
  use {'hrsh7th/cmp-cmdline'}
  use {'hrsh7th/nvim-cmp',
  config = function() require('tpb.plugins.cmp')end}
  use {'hrsh7th/cmp-nvim-lsp-signature-help'}

  -- Snippets
  use {'rafamadriz/friendly-snippets'}
  use {'saadparwaiz1/cmp_luasnip'}
  use {'L3MON4D3/LuaSnip', config = function()
    require("luasnip.loaders.from_vscode").lazy_load()
    require('tpb.plugins.snippets') end}

  -- Notes
  use {'renerocksai/telekasten.nvim',
  disable = false, --https://github.com/renerocksai/telekasten.nvim/issues/106
  config = function() require('telekasten').setup({
    home = '/Users/Tim.Bump/zettelkasten',
    dailies = '/Users/Tim.Bump/zettelkasten/daily'}) end}

  -- Editor
  use {'windwp/nvim-autopairs',
  config = function() require('nvim-autopairs').setup{} end }
  use {'terrortylor/nvim-comment',
  config = function() require('nvim_comment').setup() end}

  -- UI
  use {'kyazdani42/nvim-web-devicons'}
  use {'lukas-reineke/indent-blankline.nvim',
  config = function() require("indent_blankline").setup {
    show_current_context = true,
  } end}
  use {'kyazdani42/nvim-tree.lua', -- maybe nnn.nvim
  cmd = 'NvimTreeToggle', -- maybe nnn.nvim
  config = function() require('nvim-tree').setup{} end}
  use {
    "folke/zen-mode.nvim",
    config = function()
      require("zen-mode").setup {}
    end
  }
  use {'nvim-lualine/lualine.nvim',
  config = function() require('lualine').setup {options = { theme = 'nightfly'}} end}

  -- Themes
  use {'ishan9299/modus-theme-vim'}
  use {'folke/tokyonight.nvim'}
  use {'shaunsingh/seoul256.nvim'}
  use {'shaunsingh/nord.nvim'}
  use {'windwp/wind-colors'}
  use {'sainnhe/gruvbox-material'}
  use {'savq/melange'}
  use {'Mofiqul/vscode.nvim'}
  -- REVIEW: https://github.com/wbthomason/packer.nvim/issues/858
  use {'https://gitlab.com/__tpb/nvim-selenized-lua'}

  -- VCS
  use {'kdheepak/lazygit.nvim'}

  -- Language Support
  -- Markdown
  use {'godlygeek/tabular'}
  use {'preservim/vim-markdown',
  config = function()
    vim.g.vim_markdown_folding_disabled = 1
    vim.g.vim_markdown_frontmatter = 1
  end}
  -- Rust
  use {'simrat39/rust-tools.nvim',
  config = function() require('rust-tools').setup({}) end}
  -- SQL
  use {'tpope/vim-dadbod'}
  use {'kristijanhusak/vim-dadbod-ui'}
  use {'kristijanhusak/vim-dadbod-completion'}

  -- REVIEW:
  -- use {'jubnzv/mdeval.nvim'}
  -- use {'ellisonleao/glow.nvim'}
  -- use {'ray-x/lsp-navigator'} create new branch for testing
  -- use {'nvim-lua/lsp_extensions.nvim'}
  -- use {'jose-elias-alvarez/nvim-lsp-ts-utils'}
  -- nvim-ts-autotag
  -- ts-text-objects
  -- formatter
  -- use {'gennaro-tedesco/nvim-jqx'}
  -- nvim dap /dap-ui
end)
