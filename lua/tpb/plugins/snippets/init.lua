local ls = require("luasnip")
-- some shorthands...
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local l = require("luasnip.extras").lambda
local rep = require("luasnip.extras").rep
local p = require("luasnip.extras").partial
local m = require("luasnip.extras").match
local n = require("luasnip.extras").nonempty
local dl = require("luasnip.extras").dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local types = require("luasnip.util.types")
local conds = require("luasnip.extras.expand_conditions")

-- Markdown
ls.add_snippets("markdown", {
  s("sqltbl",
  fmt(
  [[
 ---
 title: {}.{}.{}
 tags:
   - sql
   - table
 ---
 ## Description
 {}

 ## Structure

 ```sql
 USE {}
 SELECT COLUMN_NAME,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH
 FROM INFORMATION_SCHEMA.COLUMNS
 WHERE TABLE_SCHEMA = '{}'
 AND TABLE_NAME='{}'
 ```
  ]], {
    i(1, "Database"),
    i(2, "Schema"),
    i(3, "Table"),
    i(0, ""),
    rep(1),
    rep(2),
    rep(3)})),

  s("hub",
  fmt(
[[
---
title: {}
tags:
  - hub
---

See backlinks for related notes.
{}
]], {
  i(1, ""),
  i(0, "")}))

})
