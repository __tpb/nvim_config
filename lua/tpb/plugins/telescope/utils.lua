-- Create Telescope Commands
local M = {}
local t = require('telescope.builtin.files')
-- String_grep for references to current file name
M.grep_filerefs = function(args)
  local file = vim.fn.expand('%:t:r')
  local opts = {}
  opts = args or {search = file}
  t.grep_string(opts)
end
-- String_grep for links to current file
M.grep_backlinks = function(args)
  local file = string.char(92) .. string.char(40) .. '.*' .. vim.fn.expand('%:t') .. string.char(41)
  local opts = {}
  opts = args or {search = file, use_regex = true}
  t.grep_string(opts)
end
-- String_grep for selection
-- String_grep for input
--
--
-- Create Telescope Actions
-- Copy file location of result
-- Insert file location of result
return M
